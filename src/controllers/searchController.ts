import { Request, Response } from "express";

export const cachorro = (req:Request, res:Response)=>{
    res.render('cachorros');
}
export const gato = (req:Request, res:Response)=>{
    res.render('gatos');
}
export const peixe = (req:Request, res:Response)=>{
    res.render('peixes');
}
export const search = (req:Request, res:Response)=>{
    res.render('search');
}