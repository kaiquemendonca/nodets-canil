import express, { Request, Response } from "express";
import mustache from "mustache-express";
import dotenv from 'dotenv';
import rota from './routes/index';
import path from 'path';

dotenv.config();

const mainRota = rota;
const server = express();

server.set('view engine', 'mustache');
server.set('views', path.join(__dirname,'views'));
server.use(express.static(path.join(__dirname,'../public')));


server.engine('mustache', mustache());

rota.use((req: Request, res:Response)=>{
    res.status(404).send('Página não encontrada!');
});

server.use(mainRota);

server.listen(process.env.PORT);