import { Router, Response, Request } from "express";
import * as homeController from '../controllers/homeController';
import * as searchController from '../controllers/searchController';

const rota = Router();

rota.get('/', homeController.home);
rota.get('/cachorros',searchController.cachorro);
rota.get('/gatos',searchController.gato);
rota.get('/peixes',searchController.peixe);
rota.get('/search',searchController.search);

export default rota;

